let aTagArray = document.getElementsByTagName('a')
let completed = []

for (let a of aTagArray) {
    completed.push(false)
}


for (let [index, value] of [...aTagArray].entries()) {
    value.addEventListener('mouseover', () => {

        if (completed[index] === true) {
            return
        }

        let href = value.href
        let innerHtml = value.innerHTML
    
        let firstChar = href.indexOf('(')
        let secondChar = href.indexOf(')')
    
        let resultString = innerHtml + ' ' + href.substring(firstChar + 1, secondChar)
    
        value.innerHTML = resultString

        completed[index] = true
    })
}