let nameInput = document.querySelector('#name')
let commentContent = document.querySelector('#commentContent')
let sendCommentButton = document.querySelector('#send-comment')
let articleInner = document.querySelector('.article-inner')

commentContent.setSelectionRange(0, 0);
commentContent.focus();

sendCommentButton.addEventListener('click', () => {
    let answerDiv = document.createElement('div')
    let answererNameDiv = document.createElement('div')
    let answerDateDiv = document.createElement('div')
    let answerCommentDiv = document.createElement('div')
    
    answerDiv.classList.add('answer')
    answererNameDiv.innerHTML = nameInput.value
    answererNameDiv.classList.add('answerer-name')
    answerDateDiv.classList.add('answer-date')
    answerDateDiv.innerHTML = `${new Date().getDay()}.${new Date().getMonth() + 1}.${new Date().getFullYear()}`
    answerCommentDiv.innerHTML = commentContent.value
    answerCommentDiv.classList.add('comment')
    answerCommentDiv.appendChild(
        document.createElement('hr')
    )

    answerDiv.appendChild(answererNameDiv)
    answerDiv.appendChild(answerDateDiv)
    answerDiv.appendChild(answerCommentDiv)

    articleInner.appendChild(answerDiv)

    nameInput.value = ''
    commentContent.value = ''
})