let imgs = document.getElementsByTagName('img')
let resultDiv = document.querySelector('#result')


for (let value of imgs) {
    value.addEventListener('click', (e) => {
        resultDiv.innerHTML = e.currentTarget.src
    })
}
