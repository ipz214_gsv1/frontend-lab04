let inputs = document.getElementsByTagName('input')

for (let input of inputs) {
    input.addEventListener('mouseout', () => {
        let dataLength = Number(input.dataset.length)

        console.log(dataLength)

        let value = input.value

        if (value.length > dataLength)
            input.style.borderColor = 'red'
        else 
            input.style.borderColor = 'green'
    })
}