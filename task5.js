let firstname = "firstname"
let lastname = "lastname"
let patronymic = "patronymic"

let elemDiv = document.getElementById('elem')
let addClassButton = document.getElementById('addClass')
let outputButton = document.getElementById('output')
let resultCountDiv = document.getElementById('resultCount')
let resultItemsDiv = document.getElementById('resultItems')

addClassButton.addEventListener('click', () => {
    elemDiv.classList.toggle(firstname)
})

outputButton.addEventListener('click', () => {
    resultCountDiv.innerHTML = elemDiv.classList.length

    resultItemsDiv.innerHTML = ''

    for (let i = 0; i < elemDiv.classList.length; i++) {
        resultItemsDiv.innerHTML = resultItemsDiv.innerHTML + `\n ${elemDiv.classList[i]}` 
    }
})

elemDiv.classList.add(firstname, lastname, patronymic)

elemDiv.classList.remove(firstname)

let checkIfExists = elemDiv.classList.contains(firstname)


