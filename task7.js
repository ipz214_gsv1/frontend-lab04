let headers = document.querySelectorAll('.header')

let alreadyChoosen = false

for (let header of headers) {
    header.addEventListener('click', (e) => {
        if (e.currentTarget.nextElementSibling.style.display === 'block') {
            e.currentTarget.nextElementSibling.style.display = 'none'
            alreadyChoosen = false
        } else {
            if (!alreadyChoosen) {
                e.currentTarget.nextElementSibling.style.display = 'block'
                alreadyChoosen = true
            }
        }
    })
}