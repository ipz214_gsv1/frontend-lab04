let smallSquares = document.querySelectorAll('.small-square')
let square = document.querySelector('.square')
let addButton = document.querySelector('#addButton')

for (let smallSquare of smallSquares) {
    smallSquare.addEventListener('click', (e) => {
        e.currentTarget.remove()
    })
}

addButton.addEventListener('click', () => {
    let randomColor = Math.floor(Math.random()*16777215).toString(16);
    let div = document.createElement('div')
    div.classList.add('small-square')

    div.setAttribute('style', `background-color: #${randomColor} !important`)

    square.appendChild(div)

    smallSquares = document.querySelectorAll('.small-square')
    addEventListeners(smallSquares)
})

const addEventListeners = (items) => {
    for (let smallSquare of items) {
        smallSquare.addEventListener('click', (e) => {
            e.currentTarget.remove()
        })
    }
}

