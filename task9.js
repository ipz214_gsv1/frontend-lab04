let colorSquares = document.querySelectorAll('.color-square')
let contentDiv = document.querySelector('.content')
let fontStyles = document.querySelectorAll('.fontStyle')
let fontSizeInput = document.getElementsByName('fontSize')[0]
let fontSelect = document.getElementsByName('fontSelect')[0]

contentDiv.style.fontFamily = 'Arial'

let fonts = ["Arial", "Verdana", "Oswald", "Tahoma", "Trebuchet MS", "Times New Roman", "Georgia",
             "Garamond", "Courier New", "Brush Script MT"]

for (let font of fonts) {
    let opt = document.createElement('option')
    opt.innerHTML = font
    opt.setAttribute('value', `${font}`)
    fontSelect.appendChild(opt)
}

console.log(contentDiv.fontFamily)


fontSelect.addEventListener('change', (e) => {
    contentDiv.style.fontFamily = e.currentTarget.value
})


for (let colorSquare of colorSquares) {
    let randomColor = Math.floor(Math.random()*16777215).toString(16);
    colorSquare.setAttribute('style', `background-color: #${randomColor} !important`)

    colorSquare.addEventListener('click', (e) => {
        contentDiv.style.color = e.currentTarget.style.backgroundColor
    })
}

fontSizeInput.addEventListener('change', (e) => {
    contentDiv.style.fontSize = `${Number(e.currentTarget.value)}px`
})

for (let fontStyle of fontStyles) {
    console.log('asdasd')
    fontStyle.addEventListener('click', (e) => {
        let type = e.currentTarget.innerHTML.replace(/ /g,'')
        console.log(type)

        if (type.includes('B')) 
            contentDiv.style.fontWeight = '700'

        if (type.includes('I'))
            contentDiv.style.fontStyle = 'italic' 

        if (type.includes('U'))
            contentDiv.style.textDecoration = 'underline' 
    })
}