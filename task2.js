let paragraphs = document.getElementsByTagName('p')

console.log(paragraphs)

for (let p of paragraphs) {
    p.addEventListener('click', (e) => {
        let value = Number(e.currentTarget.innerHTML)

        e.currentTarget.innerHTML = (value ** 2).toString()
    })
}